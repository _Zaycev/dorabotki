function createCurrency(list) { 
    $('#ammount').val(1000);  
    list.map(function(i){
        return $('<option/>').html(i.Name + ' ('+ i.CharCode + ')').data('country', i.CharCode).val(i.Value).appendTo('#current-currency, #new-currency');    
    });      
    $('#new-currency option').each(function(){
        if ($(this).data('country') == 'USD') {
            $(this).attr('selected', 'selected').prop('selected', true); 
        }
    });
} 
$.get('http://university.netology.ru/api/currency', createCurrency);

$('form').bind('change click keyup', function (){
    var ammount = $('#ammount').val();
    var currentCurrency = $('#current-currency').val();
    var newCurrency = $('#new-currency').val();                
    ammount = ammount.replace(',', '\.').replace(' ', '');        
    
    var result = currentCurrency * ammount / newCurrency;
    result = result.toFixed(2).toString().replace('\.', ',');
    $('#result').html(result + $('#new-currency option:selected').data('country').replace('', '  ')); 
});
 







/*$.ajax({
    url: 'http://university.netology.ru/api/currency',
    crossDomain: true,
    dataType: 'json',
    success: function (data) {
        $('#ammount').val(1000);         
        $(data).each(function(i) {
            $('#current-currency').append($('<option data-country='+data[i].CharCode+' value='+ data[i].Value+'>'+data[i].Name + ' ('+ data[i].CharCode + ') '+'</option>'));
            $('#new-currency').append($('<option data-country='+data[i].CharCode+' value='+ data[i].Value+'>'+data[i].Name + ' ('+ data[i].CharCode + ') '+'</option>'));
        });      
        $('#new-currency option').each(function(){
            if ($(this).data('country') == 'USD') {
                $(this).attr('selected', 'selected').prop('selected', true); 
            }
        });
    },
    error: function(){
        alert('Не удалось загрузить курсы валют');
    }
});
*/ 

/*$('<option/>', {
   'data-country': data[i].CharCode,
    text: data[i].Name + ' ('+ data[i].CharCode + ')',
    val: data[i].Value 
}).appendTo('#current-currency, #new-currency');*/